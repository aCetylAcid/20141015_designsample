//
//  CustomMapViewController.m
//  20141015_DesignSample
//
//  Created by aCetylAcid on 2014/10/16.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import "CustomMapViewController.h"

@interface CustomMapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property NSTimer *commonTimer;
@end

@implementation CustomMapViewController

- (void)viewDidLoad
{
    _mapView.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(37.0f, 137.0f);
    CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithCoordinate:coordinate];
    annotation.title = @"ぐりぐりぐるぐる";
    
    [_mapView addAnnotation:annotation];
    [_mapView showAnnotations:@[annotation] animated:NO];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKAnnotationView *av = [[MKAnnotationView alloc] init];
    
    [av setImage:[UIImage imageNamed:@"pin"]];
    [av setFrame:CGRectMake(0, 0, 60, 60)];
    av.canShowCallout = YES;
    
    UIButton *btnReserve = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnReserve setImage:[UIImage imageNamed:@"btn_reserve"] forState:UIControlStateNormal];
    [btnReserve setFrame:CGRectMake(0, 0, 55, 30)];
    
    [av setRightCalloutAccessoryView:btnReserve];
    return av;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ( [view.annotation isKindOfClass:CustomAnnotation.class] ) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            ((CustomAnnotation*)view.annotation).subtitle = @"";
        });
        _commonTimer = [NSTimer scheduledTimerWithTimeInterval:0.8f
                                                        target:self
                                                      selector:@selector(updateSubtitle:)
                                                      userInfo:nil
                                                       repeats:NO];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if ( [view.annotation isKindOfClass:CustomAnnotation.class] ) {
        if ( _commonTimer ) {
            [_commonTimer invalidate];
        }
        ((CustomAnnotation*)view.annotation).subtitle = nil;
    }
}

- (void)updateSubtitle:(NSTimer*)timer
{
    dispatch_async(dispatch_get_main_queue(), ^ {
        [[_mapView selectedAnnotations].firstObject setSubtitle:@"わくわくまで30分"];
    });
}

@end