//
//  CustomMapViewController.h
//  20141015_DesignSample
//
//  Created by aCetylAcid on 2014/10/16.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import "ViewController.h"
#import "CustomAnnotation.h"

@interface CustomMapViewController : ViewController<MKMapViewDelegate>

@end
