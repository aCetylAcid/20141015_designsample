//
//  CustomAnnotation.h
//  20141015_DesignSample
//
//  Created by aCetylAcid on 2014/10/16.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomAnnotation : NSObject<MKAnnotation>

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

-(id)initWithCoordinate:(CLLocationCoordinate2D)co;

@end
