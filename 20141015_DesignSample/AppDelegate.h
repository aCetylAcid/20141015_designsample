//
//  AppDelegate.h
//  20141015_DesignSample
//
//  Created by aCetylAcid on 2014/10/15.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

