//
//  CustomAnnotation.m
//  20141015_DesignSample
//
//  Created by aCetylAcid on 2014/10/16.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import "CustomAnnotation.h"

@interface CustomAnnotation ()

@end

@implementation CustomAnnotation


-(id)initWithCoordinate:(CLLocationCoordinate2D)co{
    _coordinate = co;
    return self;
}

@end
